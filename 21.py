__author__ = 'gmoran2017'
from tkinter import *

window = Tk()
canvas = Canvas(bg = 'white', width = 500, height = 500)
canvas.create_rectangle(100, 60, 100, 60, width = 100)
canvas.pack()
def mouse(event):
    x, y = event.x, event.y
    if x >= 50 and x <= 150 and y >=10 and y <= 110:
        print('In the rectangle')
    else:
        print('Not in the rectangle')
window.bind('<Motion>', mouse)

window.mainloop()