from tkinter import *




class CanvasGrid:
    def __init__(self):
        self.window = Tk()
        self.window.title("Grid")
        self.canvas = Canvas(self.window, width=128,
                             height=128, bg="white")
        self.display_lines(16, 0, 16, 128, "red")
        self.display_lines(0, 16, 128, 16, "blue")
        self.canvas.pack()

        self.window.mainloop()

    def display_lines(self, x1, y1, x2, y2, color):
        x_plus = x1  ## all lines are evenly spaced
        y_plus = y1
        for ctr in range(7):
            self.canvas.create_line(x1, y1, x2, y2, fill=color)
            x1 += x_plus
            x2 += x_plus
            y1 += y_plus
            y2 += y_plus


CG = CanvasGrid()
